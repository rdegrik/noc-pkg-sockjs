#!/usr/bin/env python
from distutils.core import setup
import os
from collections import defaultdict

__version__ = "1.0.0"

def get_files(prefix):
    r = []
    for root, dirnames, files in os.walk(prefix):
        x = [os.path.join(root, f) for f in files]
        if x:
            r += [(root, x)]
    return r

setup(
    name="noc-pkg-sockjs",
    version=__version__,
    description="sockjs package for NOC",
    url="https://github.com/sockjs",
    maintainer="nocproject.org",
    maintainer_email="pkg@nocproject.org",
    data_files=get_files("static/pkg/sockjs")
)
